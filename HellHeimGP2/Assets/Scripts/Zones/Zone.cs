﻿using System;
using UnityEngine;
using System.Collections.Generic;

public abstract class Zone : MonoBehaviour
{
    [SerializeField]
    protected List<BaseEnemy> zoneEnemies;
    protected IDisposable enterTrig;
    protected abstract void SetupClassNecessities();

    protected virtual void Awake()
    {
        enabled = false;
    }
    protected virtual void OnEnable()
    {
        SetupClassNecessities();
        Debug.Log("Zone enabled");
    }
    protected virtual void OnDisable()
    {
        OnExit();
    }

    public void RemoveEnemyFromZoneList(BaseEnemy removedEn)
    {
        zoneEnemies.Remove(removedEn);
        if(zoneEnemies.Count <= 0)
        {
            ZoneManager.instance.EnableNextZone();
        }
    }
    protected virtual void OnEnter()
    {
        ZoneManager.instance.DisablePreviousZone();
    }

    protected virtual void OnExit()
    {
        if (enterTrig != null)
        {
            enterTrig.Dispose();
        }
    }
}
