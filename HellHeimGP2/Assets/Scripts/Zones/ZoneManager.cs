﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneManager : Singleton<ZoneManager>
{
    public List<BaseEnemy> aEnemies = new List<BaseEnemy>();
    [SerializeField]
    public List<Zone> zones = new List<Zone>();
    public int curZone;

    private void Awake()
    {
        SetupClassNecessities();
    }
    private void Start()
    {
        zones[curZone].enabled = true;
    }

    public void EnableNextZone()
    {
        curZone++;
        zones[curZone].enabled = true;
        Debug.Log("Enabling next Zone");
    }

    public void DisablePreviousZone()
    {
        zones[curZone - 1].enabled = false;
    }

    void SetupClassNecessities()
    {
        aEnemies.AddRange(FindObjectsOfType<BaseEnemy>());
    }
}
