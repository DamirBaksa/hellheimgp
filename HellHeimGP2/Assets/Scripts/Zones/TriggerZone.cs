﻿using UniRx;
using System.Linq;
using UnityEngine;
using UniRx.Triggers;

public class TriggerZone : Zone
{
    [SerializeField]
    Transform doorWay;
    ObservableTriggerTrigger ott;

    protected override void OnEnable()
    {
        base.OnEnable();
        UnlockDoor();
    }

    protected override void OnEnter()
    {
        base.OnEnter();
        foreach (BaseEnemy enemy in zoneEnemies)
        {
            Debug.Log("Awakening Enemies");
            enemy._EnemyState.OnNext(EnemyState.Awake);
        }
    }
    protected override void SetupClassNecessities()
    {
        ott = GetComponent<ObservableTriggerTrigger>();
        if (ott == null)
        {
            ott = gameObject.AddComponent<ObservableTriggerTrigger>();
        }
        enterTrig = ott.OnTriggerEnterAsObservable().Where(c => c.tag == "Player")
                                .Subscribe(_ => OnEnter()).AddTo(this);
    }

    public void UnlockDoor()
    {
        if (doorWay != null)
        {
            Observable.EveryUpdate().Subscribe(_ =>
            {
                if (doorWay.localScale.y >= 0)
                {
                    doorWay.localScale -= new Vector3(0, 1f * Time.deltaTime, 0);
                }
                else
                {
                    doorWay.gameObject.SetActive(false);
                }
            }).AddTo(this);
        }
    }
}
