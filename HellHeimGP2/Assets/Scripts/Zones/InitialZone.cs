﻿using System.Linq;
using UniRx;

public class InitialZone : Zone
{
    public InfoDialogueObject[] zoneDialogues;

    protected override void OnEnable()
    {
        base.OnEnable();
        foreach(var dialogue in zoneDialogues)
        {
            dialogue.ObserveEveryValueChanged(d => d.isRead).Where(_ => zoneDialogues.All(d => d.isRead))

                                                            .Subscribe(_ => ZoneManager.instance.EnableNextZone());
        }

    }

    protected override void SetupClassNecessities()
    {
       // Needs to inherit this from Zone class, not used in this class
    }
}
