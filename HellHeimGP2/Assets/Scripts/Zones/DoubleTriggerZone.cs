﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
public class DoubleTriggerZone : Zone
{
    [SerializeField]
    Transform[] doorways;
    int curDoor;
    ObservableTriggerTrigger ott;
    
    protected override void OnEnable()
    {
        base.OnEnable();
        UnlockDoor();
    }

    protected override void OnEnter()
    {
        base.OnEnter();
        foreach (BaseEnemy enemy in zoneEnemies)
        {
            Debug.Log("Awakening Enemies");
            enemy._EnemyState.OnNext(EnemyState.Awake);
        }
    }
    protected override void SetupClassNecessities()
    {
        ott = GetComponent<ObservableTriggerTrigger>();
        if (ott == null)
        {
            ott = gameObject.AddComponent<ObservableTriggerTrigger>();
        }
        enterTrig = ott.OnTriggerEnterAsObservable().Where(c => c.tag == "Player")
                                .Subscribe(_ => OnEnter()).AddTo(this);
    }

    public void UnlockDoor()
    {
        if(curDoor <= doorways.Length)
        {
            Observable.EveryUpdate().Subscribe(_ =>
            {
                if (doorways[curDoor].localScale.y >= 0)
                {
                    doorways[curDoor].localScale -= new Vector3(0, 1f * Time.deltaTime, 0);
                }
                else
                {
                    doorways[curDoor].gameObject.SetActive(false);
                    if (curDoor <= doorways.Length)
                    {
                        curDoor++;
                        UnlockDoor();
                    }
                }
            }).AddTo(this);
        }
    }
}
