﻿using System.Collections.Generic;

public interface IZone  {

    void OnEnter();
    void OnExit();
}
