﻿using UnityEngine;

public interface ITargetable
{
    void OnHit(int damage);
    Vector2 GetTargetPosition();
}