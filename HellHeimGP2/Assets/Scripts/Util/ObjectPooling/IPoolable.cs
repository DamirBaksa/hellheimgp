﻿using UnityEngine;

public interface IPoolable
{
    void OnCreate(IPoolController _poolController);
    void OnSpawn(Transform spawnPos);
    void OnDespawn();
}