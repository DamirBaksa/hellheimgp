﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//I need an object to pass in that is a GAMEOBJECT with the IPoolable interface
//Example, we want to pool 'Bullet', we pass in a new instance of ObjectPooler<Bullet>()
public class ObjectPooler<T> where T : MonoBehaviour, IPoolable {

    //This is going to store a function that will be run when we create a a new instance of the poolable object
    public delegate T CreateObjectDelegate();
    public CreateObjectDelegate _creationMethod;

    //Think of this as our list of objects, expect without having to itterate through them (better for performance)
    private Stack<T> _objectStack;

    //going to be reference of the starting amount of poolable objects to create
    private int _initialSize;


    //Constructor class
    public ObjectPooler(int initialSize, CreateObjectDelegate creationMethod)
    {
        _objectStack = new Stack<T>();
        Init(initialSize, creationMethod);
    }

    private void Init(int initialSize, CreateObjectDelegate creationMethod)
    {
        _creationMethod = creationMethod;
        _initialSize = initialSize;

        for (int i = 0; i < _initialSize; i++)
        {
            //This T will get the return of instantiate of type in the method of creationMethod() delegate
            T poolObject = creationMethod();

            //Hides object
            poolObject.OnDespawn();

            //Push into stack to be used for later
            Store(poolObject);
        }
    }

    public void Store(T type)
    {
        //Push() adds into the stack, increasing _objectStack.Count
        _objectStack.Push(type);
    }

    //This defaults our param so we can call this method as Get() or as Get(false). passing just Get() defaults in this case as true;
    public T Get(bool create = true)
    {
        if (_objectStack.Count == 0)
        {
            //If we allow this object pooler to create new obejcts once it's reached it's limit, it will instantiate a new object. 
            //(or atleast is supposed to depending on delegate --> it will run _creationMethod() and do what it's told to do)
            if (create == true)
            {
                return _creationMethod();
            }

            //otherwise return null
            return null;
        }

        //Pop gives us an available item from the stack
        //Once it gets "Pop()"ed, it is no longer in the stack
        //Hence, lowering court of _objectStack.Count
        else return _objectStack.Pop();
    }
}
