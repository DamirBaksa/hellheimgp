﻿using UnityEngine;
using System;

public class PoolExample : MonoBehaviour, IPoolController {

    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    int startingPoolCount;

    private ObjectPooler<Bullet> _bulletObjectPool;

    public void DespawnPoolableObject(IPoolable obj)
    {
        _bulletObjectPool.Store((Bullet)obj);
    }

    private void Awake()
    {
        //In the param, the delegate gets called to create a new pooled object ONLY if there is none in the stact to use.
        _bulletObjectPool = new ObjectPooler<Bullet>(startingPoolCount, delegate () {
            GameObject go = Instantiate(bulletPrefab);
            Bullet bullet = go.GetComponent<Bullet>();
            bullet.OnCreate(this);
            return bullet;
        });
    }

    //private void Update()
    //{
    //    if (Input.GetKey(KeyCode.A))
    //    {
    //        //Gets a bullet from our pool, checks first if there is one to use, if not it will create on itself to be reused in the future
    //        Bullet bullet = _bulletObjectPool.Get();
    //        if (bullet != null)
    //        {
    //            bullet.OnSpawn(Vector3.zero);
    //            bullet.InitBullet(Vector3.forward, 10);
    //        }
    //    }
    //}
}
