﻿using UnityEngine;

public class Bullet : MonoBehaviour, IPoolable
{
    IPoolController _poolController;

    Vector3 _dir;
    float _moveForce;
    bool isActived = false;

    Transform rSpawn;

    public void OnCreate(IPoolController poolController)
    {
        _poolController = poolController;
    }

    public void OnDespawn()
    {
        isActived = false;
        gameObject.SetActive(false);
    }

    public void OnSpawn(Transform spawnPos)
    {
        transform.position = spawnPos.position;
        transform.rotation = spawnPos.rotation;
        gameObject.SetActive(true);
        Invoke("DestroyBullet", 3);
    }

    public void InitBullet(Vector3 dir, float moveForce)
    {
        _dir = dir;
        _moveForce = moveForce;
        isActived = true;
    }

    void DestroyBullet()
    {
        _poolController.DespawnPoolableObject(this);
        OnDespawn();
    }

    void Update()
    {
        if(isActived == true)
        {
            transform.Translate(_dir * _moveForce * Time.deltaTime);
        }
    }

}
