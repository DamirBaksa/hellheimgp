﻿using UniRx;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EnemyState
{
    Idle,
    Awake,
    Shoot,
    Dead
}

public class BaseEnemy : MonoBehaviour, IPoolController
{
    public BehaviorSubject<EnemyState> _EnemyState = new BehaviorSubject<EnemyState>(EnemyState.Idle);
    public EnemyState EnemyState { get { return _EnemyState.Value; } }

    List<IDisposable> _currentStateStreams = new List<IDisposable>(); // To track any state specific streams that need to get disposed of when the stream changes
    GameObject player;
    [SerializeField]
    GameObject bulletPrefab;
    [SerializeField]
    Transform shotSpot;
    [SerializeField]
    ParticleSystem smokePS;
    [SerializeField]
    ParticleSystem ChargeUpPS;
    [SerializeField]
    int startingBulletCount;
    float transitionTime;
    IDisposable attackColDisp;
    private ObjectPooler<Bullet> _bulletObjectPool;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //In the param, the delegate gets called to create a new pooled object ONLY if there is none in the stact to use.
        _bulletObjectPool = new ObjectPooler<Bullet>(startingBulletCount, delegate ()
        {
            GameObject go = Instantiate(bulletPrefab);
            Bullet bullet = go.GetComponent<Bullet>();
            bullet.OnCreate(this);
            return bullet;
        });

    }

    void Start()
    {
        _EnemyState.Subscribe(_ =>
        {
            _currentStateStreams.ForEach(stream => stream.Dispose());
            _currentStateStreams.Clear();
        }).AddTo(this);

        _EnemyState.Where(state => state == EnemyState.Idle).Subscribe(_ => IdleState()).AddTo(this);
        _EnemyState.Where(state => state == EnemyState.Awake).Subscribe(_ => AwakeState()).AddTo(this);
        _EnemyState.Where(state => state == EnemyState.Shoot).Subscribe(_ => ShootState()).AddTo(this);
        _EnemyState.Where(state => state == EnemyState.Dead).Subscribe(_ => DeadState()).AddTo(this);

    }

    #region StateMethods
    private void IdleState()
    {

    }
    private void AwakeState()
    {
        // Having Enemy look at player through entire Awake State
        Debug.Log("Entered Awake State");
        _currentStateStreams.Add(
            Observable.EveryUpdate().Take(TimeSpan.FromSeconds(1.5f))
                                    .DoOnCompleted(() => _EnemyState.OnNext(EnemyState.Shoot))
                                    .Subscribe(_ =>
                                    {
                                        transform.LookAt(player.transform);
                                    })
                                );
    }

    bool StateCheck(EnemyState curState)
    {
        if (_EnemyState.Value == curState)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void ShootState()
    {
        Debug.Log("Entered Shoot State");
        Observable.EveryUpdate().Where(_ => StateCheck(EnemyState.Shoot)).Subscribe(Z =>
        {
            transform.LookAt(player.transform);
        });
        StartCoroutine(Shoot());
    }

    IEnumerator Shoot()
    {
        while (_EnemyState.Value == EnemyState.Shoot)
        {
            Bullet bullet = _bulletObjectPool.Get();
            if (bullet != null)
            {
                bullet.OnSpawn(shotSpot);
                bullet.InitBullet(Vector3.forward, 10);
            }
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.4f, 1.4f));
        }
    }
    private void DeadState()
    {
        VitalsUI.instance.AdjustMana(10);
        ZoneManager.instance.zones[ZoneManager.instance.curZone].RemoveEnemyFromZoneList(this);
        Destroy(gameObject);
    }

    public void DespawnPoolableObject(IPoolable obj)
    {
        _bulletObjectPool.Store((Bullet)obj);
    }
    #endregion

    public void TakeDamage()
    {
        Debug.Log("Enemy is Dead!");
        _EnemyState.OnNext(EnemyState.Dead);
    }
}
