﻿using UnityEngine;
using UniRx;
using System;
using UniRx.Triggers;

public class PlayerActions : PlayerInputListener
{
    [SerializeField]
    Collider attackCollider;
    [SerializeField]
    float moveSpeed;
    [SerializeField]
    float attackCooldown = 0.5f;

    bool canDash = true;
    protected bool isDead = false;
    Animator anim;

    ObservableTriggerTrigger octtt;
    IDisposable attackDisp;
    IDisposable attackColDisp;

    #region OnAwake
    private void Start()
    {
        SetupClassConnections();
        SetupInputReactions();
    }

    void RayCastCollisionCheck()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward, out hit, 5))
        {
            if(hit.transform.tag == "Barrier" || hit.transform.tag == "TriggerEntrance")
            {
                Debug.Log("Wall in Range, Can't dash!");
                canDash = false;
            }
            else
            {
                canDash = true;
            }
        }
        else
        {
            canDash = true;
        }

    }

    void SetupClassConnections()
    {

        if (octtt == null)
        {
            octtt = attackCollider.gameObject.AddComponent<ObservableTriggerTrigger>();
        }
        else
        {
            octtt = attackCollider.gameObject.GetComponent<ObservableTriggerTrigger>();
        }

        anim = GetComponentInChildren<Animator>();
    }
    void SetupInputReactions()
    {
        moveStream.Where(a => a).Subscribe(_ => Move());
        attackStream.Where(q => q).Subscribe(_ => Attack());
        dashStream.Where(_ => _).Subscribe(_ => Dash());

    }
    #endregion

    private void Update()
    {
        if (!isDead)
        {
            anim.SetBool("isRunning", curLoc != prevLoc);
            if(curLoc != prevLoc)
            {
                FaceMovementDirection(); // Function in InputListener class
            }
            if (mana <= 0)
            {
                canDash = false;
            }
        }
    }

    #region InputReactions
    void Move()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        BaseMove();
        curLoc += new Vector3(horizontalInput * moveSpeed, 0, verticalInput * moveSpeed) * Time.fixedDeltaTime;
        transform.position = curLoc; 
    }
    void Dash()
    {
        RayCastCollisionCheck();
        if(canDash)
        {
             anim.SetTrigger("dash");
             transform.Translate(Vector3.forward * 4);
             VitalsUI.instance.AdjustMana(-10);
        }
    }

    #region Attacking
    protected void Attack()
    {
        // Disposable that Runs The Attack start code and the attack end code based on an Interval of attackCooldown.
        // Made it all one stream cuz its fun
        attackDisp = Observable.Interval(TimeSpan.FromSeconds(attackCooldown))
             .DoOnSubscribe(delegate
             {
                 Debug.Log("Attack Start");
                 anim.SetTrigger("attack");
                 attackCollider.enabled = true;
                    // Activating sword Trigger detection to do damage to enemies
                 attackColDisp = octtt.OnTriggerEnterAsObservable().Select(c => c.gameObject.GetComponent<BaseEnemy>()).Where(bEn => bEn != null)
                                                     .Subscribe(bEn =>
                                                     {
                                                         Debug.Log("COLLISION ENTER");
                                                         bEn.TakeDamage();
                                                     });
             })
            .Subscribe(delegate
            {
                Debug.Log("ATTACK END");
                attackCollider.enabled = false;

                    // Disposing of temporary Dispoables
                attackDisp.Dispose();
                attackColDisp.Dispose();
            });
    }
    #endregion

    #endregion

    public override void Die()
    {
        isDead = true;
    }
}
