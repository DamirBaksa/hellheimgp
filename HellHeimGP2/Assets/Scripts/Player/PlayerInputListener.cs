﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class PlayerInputListener : MonoBehaviour
{
                /****** Variables**********/

    // Movementlistener
    [HideInInspector]
    protected IObservable<bool> moveStream;
    // Actionlisteners
    [HideInInspector]
    protected IObservable<bool> attackStream, dashStream;
    // CollisionListener
    ObservableCollisionTrigger oct;

    // Vectors
    protected Vector3 curLoc;
    protected Vector3 prevLoc;
    // Floats
    [HideInInspector]
    public float health, maxHealth = 100;
    public float mana, maxMana = 100;
    public float lookSpeed = 10;
                /**************************/
    #region OnAwake
    private void Awake()
    {
        SetupInputListeners();

        health = maxHealth;
        mana = maxMana;
    }
    void SetupInputListeners()
    {
        moveStream = Observable.EveryUpdate().Select(_ => Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0);
        attackStream = Observable.EveryUpdate().Select(_ => Input.GetKeyDown(KeyCode.Q));
        dashStream = Observable.EveryUpdate().Select(_ => Input.GetKeyDown(KeyCode.Space));

        if(oct == null)
        {
            oct = gameObject.AddComponent<ObservableCollisionTrigger>();
        }
        else
        {
            oct = GetComponent<ObservableCollisionTrigger>();
        }

        oct.OnCollisionEnterAsObservable().Select(_ => _.gameObject.GetComponent<Bullet>()).Where(_ => _ != null)
                                            .Subscribe(c =>
                                            {
                                                Destroy(c.gameObject);
                                                VitalsUI.instance.AdjustHealth(-10);
                                            });
    }
    #endregion

    protected void FaceMovementDirection()
    {
        var rot = Quaternion.Lerp(transform.rotation, 
                             Quaternion.LookRotation(transform.position - prevLoc), 
                                                    Time.fixedDeltaTime * lookSpeed).eulerAngles;

        rot.x = 0;
        rot.z = 0;

        transform.eulerAngles = rot;
    }
    protected void BaseMove()
    {
        prevLoc = curLoc;
        curLoc = transform.position;
    }
    public virtual void Die()
    {

    }
}
