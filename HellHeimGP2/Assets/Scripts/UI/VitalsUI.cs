﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class VitalsUI : Singleton<VitalsUI>
{
    PlayerInputListener playerInput;
    public Image healthBar;
    public Image manaBar;

    private void Awake()
    {
        playerInput = FindObjectOfType<PlayerInputListener>();
    }

    public void AdjustHealth(float amount)
    {
            // Remove health
        if (amount < 0)
        {
            if(playerInput.health <= 0)
            {
                playerInput.Die();
            }
            else
            {
                playerInput.health += amount;
                StartCoroutine(AdjustVitalBar(healthBar, amount));
            }
        }

            // Add health 
        if(amount > 0)
        {
            if(playerInput.health >= playerInput.maxHealth)
            {
                Debug.Log("You're at Max HP!");
            }
            else
            {
                playerInput.health += amount;
                StartCoroutine(AdjustVitalBar(healthBar, amount));
            }
        }
    }

    public void AdjustMana(float amount)
    {
        // Remove mana
        if (amount < 0)
        {
            if (playerInput.mana <= 0)
            {
                Debug.Log("Out of Mana!");
            }
            else
            {
                playerInput.mana += amount;
                StartCoroutine(AdjustVitalBar(manaBar, amount));
            }
        }

        // Add mana
        if (amount > 0)
        {
            if (playerInput.mana >= playerInput.maxMana)
            {
                Debug.Log("You have Max Mana!");
            }
            else
            {
                playerInput.mana += amount;
                StartCoroutine(AdjustVitalBar(manaBar, amount));
            }
        }
    }

    IEnumerator AdjustVitalBar(Image bar, float amount)
    {
        bar.fillAmount += amount / 100;
        yield return null;
    }
}
