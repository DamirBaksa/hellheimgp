﻿using UnityEngine;
using System;
using UniRx;
using TMPro;

public class UIPlayerDialogue : MonoBehaviour
{
    public GameObject readCheckPanel;
    public TMP_Text dialogueText;
    IDisposable dialogueDisp;
    IDisposable initDisp;

    ReactiveProperty<bool> reactBool = new ReactiveProperty<bool>();
    public void EnterDialogue(string text, KeyCode key, InfoDialogueObject curDial)
    {
        if (initDisp != null || dialogueDisp != null )
        {
            return;
        }

        SetDialoguePanel(true);

        initDisp = Observable.EveryUpdate()
                               .Where(_ => Input.GetKeyDown(KeyCode.Tab)).First()
                               .Subscribe(_ =>
                               {
                                   dialogueText.text = text;
                                   dialogueDisp = Observable.EveryUpdate()
                                                        .Where(__ => Input.GetKeyDown(key)).First()
                                                        .DoOnCompleted(() => dialogueDisp = null)
                                                        .Subscribe(__ =>
                                                        {
                                                            SetDialoguePanel(false);
                                                            curDial.isRead = true;
                                                            dialogueDisp.Dispose();
                                                            dialogueDisp = null;
                                                        }).AddTo(this);
                                   initDisp.Dispose();
                                   initDisp = null;
                               }).AddTo(this);
    }
    public void ExitDialogue()
    {
        if (initDisp != null)
        {
            SetDialoguePanel(false);
            initDisp.Dispose();
            initDisp = null;
        }
    }
    void SetDialoguePanel(bool isOn)
    {
        if(isOn)
        {
            dialogueText.text = "'Tab' to Read"; 
            readCheckPanel.SetActive(true);
        }
        else
        {
            readCheckPanel.SetActive(false);
        }
    }
}
