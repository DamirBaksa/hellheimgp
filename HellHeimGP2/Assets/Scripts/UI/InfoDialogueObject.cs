﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class InfoDialogueObject : MonoBehaviour
{
    [HideInInspector]
    public Transform player;
    [SerializeField]
    string interactText;
    [SerializeField]
    KeyCode interactKey;

    public bool isRead;
    UIPlayerDialogue playerUI;
    ObservableTriggerTrigger ott;

    #region OnAwake
    private void Awake()
    {
        SetupClassConnections();
        SetupObservables();
    }
    void SetupClassConnections()
    {
            // Adding or Getting OTT component
        ott = GetComponent<ObservableTriggerTrigger>();
        if (ott == null)
        {
            ott = gameObject.AddComponent<ObservableTriggerTrigger>();
        }

        playerUI = FindObjectOfType<UIPlayerDialogue>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void SetupObservables()
    {
            // if collision object's tag is player, subscribe to EnterDialogue()'s function
        ott.OnTriggerEnterAsObservable().Where(c => c.tag == "Player")
                                        .Subscribe(scr => playerUI.EnterDialogue(interactText, interactKey, this)).AddTo(this);

        ott.OnTriggerExitAsObservable().Where(c => c.tag == "Player") 
                                       .Subscribe(scr => playerUI.ExitDialogue()).AddTo(this);
    }
    #endregion
}
